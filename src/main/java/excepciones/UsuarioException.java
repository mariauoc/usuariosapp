
package excepciones;

/**
 *
 * @author Maria del Mar
 */
public class UsuarioException extends Exception {

    public UsuarioException(String message) {
        super(message);
    }
    
}
