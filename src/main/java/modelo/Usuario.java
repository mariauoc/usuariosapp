
package modelo;

import java.util.Objects;

/**
 *
 * @author Maria del Mar
 */
public class Usuario {
    private String nombre;
    private String password;
    private int edad;

    public Usuario(String nombre) {
        this.nombre = nombre;
    }

    public Usuario(String nombre, String password, int edad) {
        this.nombre = nombre;
        this.password = password;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        return nombre.equalsIgnoreCase(other.nombre);
    }
    
    

    @Override
    public String toString() {
        return "Usuario{" + "nombre=" + nombre + ", password=" + password + ", edad=" + edad + '}';
    }
    
    
}
