
package controlador;

import excepciones.UsuarioException;
import java.util.ArrayList;
import modelo.Usuario;

/**
 *
 * @author Maria del Mar
 */
public class Controlador {
    
    private static ArrayList<Usuario> usuarios = new ArrayList<>();
    
    public static void altaUsuario(Usuario u) throws UsuarioException  {
        if (usuarios.contains(u)) {
            throw new UsuarioException("Ya existe un usuario con ese nombre");
        }
        usuarios.add(u);
    }

    public static Usuario getUsuarioByNombre(String nombre) throws UsuarioException {
        if (!usuarios.contains(new Usuario(nombre))) {
            throw new UsuarioException("No existe un usuario con ese nombre");
        }
        return usuarios.get(usuarios.indexOf(new Usuario(nombre)));
    }
    
    public static ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }
    
}
